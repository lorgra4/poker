defmodule PokerTest do
  use ExUnit.Case
  doctest Poker

  describe "straight flush" do
    test "black wins" do
      result = Poker.compare("Black: 3H 4H 5H 6H 7H White: 2S 8S AH QS 3S")
      assert result == "Black wins - straight flush"
    end

    test "white wins" do
      result = Poker.compare("Black: 2S 8S AH QS 3S White: 8S 9S TS JS QS")
      assert result == "White wins - straight flush"
    end

    test "black wins by highest card" do
      result = Poker.compare("Black: AH KH QH JH TH White: 8S 9S TS JS QS")
      assert result == "Black wins - high card Ace"
    end
  end

  describe "four of a kind" do
    test "black wins" do
      result = Poker.compare("Black: 2H 4S 4C 4D 4H White: 2S 8S AH QS 3S")
      assert result == "Black wins - four of a kind"
    end

    test "white wins" do
      result = Poker.compare("Black: 2S 8S AH QS 3S White: 2H 4S 4C 4D 4H")
      assert result == "White wins - four of a kind"
    end

    test "black wins by highest four of a kind" do
      result = Poker.compare("Black: 8S 8S 8H 8S QD White: 2H 2S 2C 2D QH")
      assert result == "Black wins - high card 8"
    end
  end

  describe "full house" do
    test "black wins" do
      result = Poker.compare("Black: 2H 4S 2C 4D 4H White: 2S 8S AH QS 3S")
      assert result == "Black wins - full house"
    end

    test "white wins" do
      result = Poker.compare("Black: 2S 8S AH QS 3S White: 2H 4S 4C 2D 4H")
      assert result == "White wins - full house"
    end

    test "black wins by highest three of a kind" do
      result = Poker.compare("Black: 9S 9S 9H QS QD White: 2H 2S 2C QD QH")
      assert result == "Black wins - high card 9"
    end
  end

  describe "flush" do
    test "black wins" do
      result = Poker.compare("Black: 2H 4H 5H 9H KH White: 2S 8S AH QS 3S")
      assert result == "Black wins - flush"
    end

    test "white wins" do
      result = Poker.compare("Black: 2S 8S AH QS 3S White: 3S 4S 7S JS QS")
      assert result == "White wins - flush"
    end

    test "black wins by highest card" do
      result = Poker.compare("Black: 2S 5S 6S 8S KS White: 3S 4S 7S JS QS")
      assert result == "Black wins - high card King"
    end
  end

  describe "straight" do
    test "black wins" do
      result = Poker.compare("Black: 2H 3H 4S 5S 6H White: 2S 8S AH QS 3S")
      assert result == "Black wins - straight"
    end

    test "white wins" do
      result = Poker.compare("Black: 2S 8S AH QS 3S White: 3H 4S 5H 6S 7S")
      assert result == "White wins - straight"
    end

    test "black wins by highest card" do
      result = Poker.compare("Black: 7H 8H 9S TS JH White: 2H 3H 4S 5S 6H")
      assert result == "Black wins - high card Jack"
    end
  end

  describe "three of a kind" do
    test "black wins" do
      result = Poker.compare("Black: 2H 4S 4C 4D 7H White: 2S 8S AH QS 3S")
      assert result == "Black wins - three of a kind"
    end

    test "white wins" do
      result = Poker.compare("Black: 2S 8S AH QS 3S White: 2H 4S 4C 4D 7H")
      assert result == "White wins - three of a kind"
    end

    test "black wins by highest three of a kind" do
      result = Poker.compare("Black: 8S 8S 8H QS AD White: 2H 2S 2C 5D QH")
      assert result == "Black wins - high card 8"
    end
  end

  describe "two pairs" do
    test "black wins" do
      result = Poker.compare("Black: 2H 2S 4C 4D KH White: 2S 8S AH QS 3S")
      assert result == "Black wins - two pairs"
    end

    test "white wins" do
      result = Poker.compare("Black: 2S 8S AH QS 3S White: 2H 2S 3C 4D 4H")
      assert result == "White wins - two pairs"
    end

    test "black wins by highest pair" do
      result = Poker.compare("Black: 8S 8S AH QS QD White: 2H 2S 5C 5D QH")
      assert result == "Black wins - high card Queen"
    end

    test "white wins by second highest pair" do
      # This is not correct yet
      result = Poker.compare("Black: 3S 3D AH QS QD White: 2H 5S 5C QD QH")
      assert result == "White wins - high card 5"
    end
  end

  describe "pair" do
    test "black wins" do
      result = Poker.compare("Black: 2H 4S 8C AD AH White: 2S 8S AH QS 3S")
      assert result == "Black wins - pair"
    end

    test "white wins" do
      result = Poker.compare("Black: 2S 8S AH QS 3S White: 2H 2S 4C 5D QH")
      assert result == "White wins - pair"
    end

    test "black wins by highest pair" do
      result = Poker.compare("Black: 8S 8S AH QS 2D White: 2H 2S 4C 5D QH")
      assert result == "Black wins - high card 8"
    end
  end

  describe "high card" do
    test "black wins" do
      result = Poker.compare("Black: 2H AS 8C JD 4H White: 2S 8S KH QS 3S")
      assert result == "Black wins - high card Ace"
    end

    test "white wins" do
      result = Poker.compare("Black: 2S 8S JH TS 3S White: 2H 3S 9C 4D QH")
      assert result == "White wins - high card Queen"
    end

    test "black wins with the second highest card" do
      result = Poker.compare("Black: 2H AS 8C JD KH White: 2S 8S AH QS 3S")
      assert result == "Black wins - high card King"
    end

    test "white wins with the last highest card" do
      result = Poker.compare("Black: 2H 7S 8C 9D TH White: 3H 7S 8C 9D TH")
      assert result == "White wins - high card 3"
    end

    test "Tie" do
      result = Poker.compare("Black: 3H 7S 8C 9D TH White: 3H 7S 8C 9D TH")
      assert result == "Tie"
    end
  end
end
