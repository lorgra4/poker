defmodule Poker.Match do
  defmodule Hand do
    defstruct [:score_value, :highest]
  end

  def calculate_score(hand) do
    is_flush = is_flush(hand) # Only check once
    is_straight = is_straight(hand)

    cond do
      is_flush && is_straight ->
        %Hand{ score_value: 9, highest: hd(hand).rank_value }
      is_flush ->
        %Hand{ score_value: 6, highest: hd(hand).rank_value }
      is_straight ->
        %Hand{ score_value: 5, highest: hd(hand).rank_value }
      true -> # this is like a `else` block
        hand
        |> Enum.reduce(%{}, fn(x, acc) -> Map.update(acc, x.rank_value, 1, &(&1 + 1))  end)
        |> Enum.sort(fn ({_, v1}, {_, v2}) -> v1 >= v2 end) # Sort by more frequent cards
        |> Enum.map(fn({k,v}) -> {k, v} end) # From a map to a list of tuples
        |> match
    end
  end

  defp is_flush(hand) do
    hand
    |> Enum.group_by(fn(x) -> x.suit end)
    |> Enum.count == 1
  end

  defp is_straight(hand) do
    hand
    |> Enum.with_index(
      fn(x, i) ->
        cond do
          i == 0 -> true # Skip the check for the first value
          true -> x.rank_value + 1 == Enum.at(hand, i - 1).rank_value
        end
      end
    )
    |> Enum.all?(fn(x) -> x == true end)
  end

  defp match([{a, 4}, _]) do
    %Hand{ score_value: 8, highest: a }
  end

  defp match([{a, 3}, {_b, 2}]) do
    %Hand{ score_value: 7, highest: a }
  end

  defp match([{a, 3}, _, _]) do
    %Hand{ score_value: 4, highest: a }
  end

  defp match([{_a, 2}, {b, 2}, {_c, 1}]) do
    %Hand{ score_value: 3, highest: b }
  end

  defp match([{a, 2}, _, _, _]) do
    %Hand{ score_value: 2, highest: a }
  end

  defp match([_, _, _, _, {a, 1}]) do
    %Hand{ score_value: 1, highest: a }
  end
end
