defmodule Poker do
  @player_and_cards 6
  @scores %{
    1 => "highest card",
    2 => "pair",
    3 => "two pairs",
    4 => "three of a kind",
    5 => "straight",
    6 => "flush",
    7 => "full house",
    8 => "four of a kind",
    9 => "straight flush"
  }

  def compare(input) do
    [ black_input, white_input ] = input
    |> String.split
    |> Enum.chunk_every(@player_and_cards)
    |> Enum.map(&delete_player_string/1)

    black_hand = Poker.Parse.parse_hand(black_input)
    white_hand = Poker.Parse.parse_hand(white_input)

    black_score = Poker.Match.calculate_score(black_hand)
    white_score = Poker.Match.calculate_score(white_hand)

    cond do
      black_score.score_value > white_score.score_value ->
        "Black wins - #{@scores[black_score.score_value]}"
      black_score.score_value < white_score.score_value ->
        "White wins - #{@scores[white_score.score_value]}"
      black_score.score_value == white_score.score_value ->
        cond do
          black_score.highest > white_score.highest ->
            "Black wins - high card #{card_names(black_score.highest)}"
          black_score.highest < white_score.highest ->
            "White wins - high card #{card_names(white_score.highest)}"
          black_score.highest == white_score.highest ->
            compare_in_tuples(black_hand, white_hand)
        end
    end
  end

  defp delete_player_string(list) do
    List.delete_at(list, 0)
  end

  defp compare_in_tuples(black_hand, white_hand) do
    tuples = Enum.map(black_hand, fn(x) -> {x.rank_value} end)

    Enum.with_index(white_hand,
      fn(y, i) ->
        Tuple.append(Enum.at(tuples, i), y.rank_value)
      end
    )
    |> compare_card
  end

  defp compare_card(tuples) do
    [{x, y} | _] = tuples # Select the first tuple

    cond do
      x > y ->
        "Black wins - high card #{card_names(x)}"
      x < y ->
        "White wins - high card #{card_names(y)}"
      x == y ->
        cond do
          Enum.count(tuples) > 1 ->
            compare_card(List.delete_at(tuples, 0)) # Remove the first and try again
          true ->
            "Tie"
        end
    end
  end

  defp card_names(card_value) do
    case card_value do
      14 -> "Ace"
      13 -> "King"
      12 -> "Queen"
      11 -> "Jack"
      other_value -> other_value
    end
  end
end
