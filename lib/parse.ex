defmodule Poker.Parse do
  defmodule Card do
    defstruct [:rank, :rank_value, :suit]
  end

  def parse_hand(list) do
    list
    |> Enum.map(&parse_card/1)
    |> Enum.sort_by(& &1.rank_value, :desc)
  end

  defp parse_card(str) do
    rank = String.at(str, 0)
    %Card{ rank: rank, suit: String.at(str, 1), rank_value: assign_rank_value(rank) }
  end

  def assign_rank_value(rank) do
    case rank do
      "A" -> 14
      "K" -> 13
      "Q" -> 12
      "J" -> 11
      "T" -> 10
      other_value -> String.to_integer other_value
    end
  end
end
